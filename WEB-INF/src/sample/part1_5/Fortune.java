package sample.part1_5;

import java.util.Random;

public class Fortune {

	private static Random random = new Random(System.currentTimeMillis());

	public String getResult(){
		int n = random.nextInt(10);
		if(n < 2){
			return "とても良いです。";
		}else if(n < 4){
			return "そこそこ良いです。";
		}else if(n < 6){
			return "普通です。";
		}else if(n < 8){
			return "悪いです。";
		}else{
			return "最悪です。";
		}
	}

}