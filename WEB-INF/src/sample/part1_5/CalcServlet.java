package sample.part1_5;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CalcServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request,HttpServletResponse response)
			throws IOException, ServletException{

		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("<html>");
		out.println("<head>");
		out.println("<title>サーブレット テスト</title>");
		out.println("</head>");
		out.println("<body>");

		out.println("<p>");
		out.println("計算します。<br>");
		int n1 = 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10;
		out.print("1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 = ");
		out.print(n1);
		out.println("<br>");
		out.println("1024 / 256 = " + 1024 / 256 + "<br>");
		out.println("</p>");

		out.println("<p>");
		out.println("今日は" + new Date() + "です。");
		out.println("</p>");

		Random random = new Random(System.currentTimeMillis());
		int n3 = random.nextInt(100);
		out.println("<p>");
		out.println("乱数：" + n3 );
		out.println("</p>");

		out.println("</body>");
		out.println("</html>");
		out.close();

	}
}
