package sample.part1_5;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FortuneServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request,HttpServletResponse response)
			throws IOException, ServletException{

		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("<html>");
		out.println("<head>");
		out.println("<title>おみくじ</title>");
		out.println("</head>");
		out.println("<body>");

		Fortune fortune = new Fortune();

		out.print("<h1>");
		out.print("あなたの運勢は");
		out.print(fortune.getResult());
		out.println("</h1>");

		out.println("</body>");
		out.println("</html>");
		out.close();

	}
}