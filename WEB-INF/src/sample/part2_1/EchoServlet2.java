package sample.part2_1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EchoServlet2 extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void doPost(HttpServletRequest request,HttpServletResponse response)
			throws IOException, ServletException{

	    request.setCharacterEncoding("UTF-8");
		String str = request.getParameter("message");

		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("<html>");
		out.println("<head>");
		out.println("<title>入力フォームから値の取得(Post)</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>");
		out.println(str);
		out.println("</h1>");
		out.println("</body>");
		out.println("</html>");
		out.close();

	}
}
